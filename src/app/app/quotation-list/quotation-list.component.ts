import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../security/auth.service";
import {QuotationDTO} from "../../model/quotationDTO";
import {QuotationServiceService} from "../../service/quotation-service.service";
import {MatTableDataSource} from "@angular/material/table";
import {ProductDTO} from "../../model/productDTO";

@Component({
  selector: 'app-quotation-list',
  templateUrl: './quotation-list.component.html',
  styleUrls: ['./quotation-list.component.css']
})
export class QuotationListComponent implements OnInit {
  quotations: QuotationDTO[] = [];
  displayedColumns: string[] = ['id','totalPrice', 'userName','operations'];
  dataSource: MatTableDataSource<QuotationDTO> = new MatTableDataSource<QuotationDTO>()

  constructor(private quotationService: QuotationServiceService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.quotationService.getAll(this.authService.getToken()).subscribe(responseFromJavaApplication => {
        this.quotations = JSON.parse(responseFromJavaApplication);
        if(this.quotations){
          console.log(this.quotations)
          this.dataSource = new MatTableDataSource(this.quotations);
        }
      },
      error => {
        alert("We had an error retrieving the data!")
        console.log("Error from server is: ");
        console.log(error)
      });
  }
delete(id: number): void{
    console.log('do nothing')
}
}
