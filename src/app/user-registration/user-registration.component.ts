import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/user.service';
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private service: UserService,
              private router: Router) {   }

  ngOnInit(): void {
  }

  onClick() {
    if (this.form){
      const createUserDTO = {
        "userName" : this.form.get('username').value,
        "password" : this.form.get('password').value
      };
      this.service.createUser(createUserDTO).subscribe(response => {
        console.log(response);
        this.router.navigate(['']);
      })
    }
    }



}
