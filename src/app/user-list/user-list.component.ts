import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserDTO } from '../model/userDTO';
import { UserService } from '../user.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {


  users: UserDTO[] = [];

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {

    this.userService.getAll().subscribe(
      responseFromJavaApplication => {
        console.log(responseFromJavaApplication);
        this.users = responseFromJavaApplication;
      },
      error => {
        alert("We had an error retrieving the data!")
        console.log("Error from server is: ");
        console.log(error)
      });
  }

  redirectToRegistration() {
    console.log("apelata");
    this.router.navigate(["/register-user"]);
  }
}

