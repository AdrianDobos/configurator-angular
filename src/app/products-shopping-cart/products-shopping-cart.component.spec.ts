import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsShoppingCartComponent } from './products-shopping-cart.component';

describe('ProductsShoppingCartComponent', () => {
  let component: ProductsShoppingCartComponent;
  let fixture: ComponentFixture<ProductsShoppingCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductsShoppingCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
