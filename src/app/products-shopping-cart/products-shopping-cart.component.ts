import { Component, OnInit } from '@angular/core';
import {ProductDTO} from "../model/productDTO";
import {ProductService} from "../service/product.service";
import {ActivatedRoute} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {DeleteProductDTO} from "../model/DeleteProductDTO";
import {AuthService} from "../security/auth.service";

@Component({
  selector: 'app-products-shopping-cart',
  templateUrl: './products-shopping-cart.component.html',
  styleUrls: ['./products-shopping-cart.component.css']
})
export class ProductsShoppingCartComponent implements OnInit {
  products: ProductDTO[] = [];
  id?: number;// id quotation
  displayedColumns: string[] = ['id','productType', 'width', 'height', 'color', 'price','operations'];
  dataSource: MatTableDataSource<ProductDTO> = new MatTableDataSource<ProductDTO>()

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private auth: AuthService) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    if (this.id) {

      this.productService.getProductsByQuotation(this.id, this.auth.getToken()).subscribe(responseFromJavaApplication => {
          console.log(responseFromJavaApplication);
          this.products = JSON.parse(responseFromJavaApplication);
          if(this.products){
            this.dataSource = new MatTableDataSource(this.products);
          }

        },
        error => {
          alert("We had an error retrieving the data!")
          console.log("Error from server is: ");
          console.log(error)
        });
  }}

  delete(id: number) : void{
    const deleteProd: DeleteProductDTO = new DeleteProductDTO();
    deleteProd.idProduct = id;
    deleteProd.idQuotation= this.id;
this.productService.deleteProduct(deleteProd,this.auth.getToken()).subscribe(res => {
  this.ngOnInit();
},
  error => {
  alert("We had an error retrieving the data!")
  console.log("Error from server is: ");
  console.log(error)
});
  }
}
