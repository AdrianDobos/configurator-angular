import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProductDTO} from "../model/productDTO";
import {DeleteProductDTO} from "../model/DeleteProductDTO";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  httpClient: HttpClient;

  constructor(client: HttpClient) {
    this.httpClient = client;
  }

  createProduct(createProduct: any , token: string | null): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.post("http://localhost:8080/api/product", createProduct, {headers, responseType: 'text' as 'json' });
  }
  getProductsByQuotation(id: number, token: string | null): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.get<any>("http://localhost:8080/api/quotation/products/" + id, {headers, responseType: 'text' as 'json' });
  }
  deleteProduct(deleteProd: DeleteProductDTO, token: string | null): Observable<any>{
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.patch<any>("http://localhost:8080/api/product/delete",deleteProd , {headers, responseType: 'text' as 'json' });
  }
}
