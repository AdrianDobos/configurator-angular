import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {QuotationDTO} from "../model/quotationDTO";

@Injectable({
  providedIn: 'root'
})
export class QuotationServiceService {
  httpClient: HttpClient;

  constructor(client: HttpClient) {
    this.httpClient = client;
  }

  createQuotation(createQuotation: any, token: string): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.post("http://localhost:8080/api/quotation", createQuotation, {headers, responseType: 'text' as 'json' });
  }

  getAll(token: string): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.get("http://localhost:8080/api/quotation", {headers, responseType: 'text' as 'json' });
  }
  getQuotation(id: number, token: string | null): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.get("http://localhost:8080/api/quotation/" + id, {headers, responseType: 'text' as 'json' });
  }
}
