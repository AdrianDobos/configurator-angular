import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { UserItemComponent } from './user-item/user-item.component';
import { UserListComponent } from './user-list/user-list.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { HomeComponent } from './home/home.component';
import { CreateQuotationComponent } from './create-quotation/create-quotation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import { ProductComponent } from './product/product.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {FlexModule} from "@angular/flex-layout";
import {MatBadgeModule} from "@angular/material/badge";
import {MatIconModule} from "@angular/material/icon";
import { ProductsShoppingCartComponent } from './products-shopping-cart/products-shopping-cart.component';
import {MatTableModule} from "@angular/material/table";
import { UserLoginComponent } from './user-login/user-login.component';
import {MatCardModule} from "@angular/material/card";
import { QuotationListComponent } from './app/quotation-list/quotation-list.component';
import { DoorsComponent } from './doors/doors.component';
import { WindowsComponent } from './windows/windows.component';
import { SlidingSystemsComponent } from './sliding-systems/sliding-systems.component';
import { RatingComponent } from './rating/rating.component';

@NgModule({
  declarations: [
    AppComponent,
    UserRegistrationComponent,
    UserItemComponent,
    UserListComponent,
    ProductListComponent,
    ProductItemComponent,
    NavigationBarComponent,
    HomeComponent,
    CreateQuotationComponent,
    ProductComponent,
    ProductsShoppingCartComponent,
    UserLoginComponent,
    QuotationListComponent,
    DoorsComponent,
    WindowsComponent,
    SlidingSystemsComponent,
    RatingComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    FlexModule,
    MatBadgeModule,
    MatIconModule,
    MatTableModule,
    MatCardModule
  ],
  providers:  [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
