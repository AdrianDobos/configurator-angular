import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserCreateDTO } from 'src/app/model/userCreateDTO';
import { UserDTO } from './model/userDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userLabel = "appUser";

  httpClient: HttpClient;

  constructor(client: HttpClient) {
    this.httpClient = client;
  }

  createUser(createUserDTO: UserCreateDTO): Observable<any> {
    console.log("register: " + createUserDTO)
    return this.httpClient.post("http://localhost:8080/api/users/signup", createUserDTO);
  }

  getAll(): Observable<any> {
    return this.httpClient.get("api/users");
  }

  getUser(token: string | null): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.get("api/users/user", {headers, responseType: 'text' as 'json' });
  }

  login(userWithCredentials: UserCreateDTO) {
    this.httpClient.post("api/users/login", userWithCredentials).subscribe(
      response => {
        const responseUser = response as UserDTO;
        const appUser: UserDTO = {
          id: responseUser.id,
          userName: responseUser.userName,
          password: userWithCredentials.password,
          profilePicture: ''
        }
        this.setUser(appUser);
      }

    )
  }

  logout() {
    localStorage.setItem(this.userLabel, '');
  }

  private setUser(user: UserDTO) {
    localStorage.setItem(this.userLabel, JSON.stringify(user));
  }

}

