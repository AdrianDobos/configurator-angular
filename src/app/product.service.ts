import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {ProductDTO } from 'src/app/model/productDTO';

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  httpClient: HttpClient;

  constructor(client: HttpClient) {
    this.httpClient = client;
  }

  createProduct(createProductDTO: ProductDTO): Observable<any> {
    return this.httpClient.post("api/products", createProductDTO);
  }

  getAll(token: string | null): Observable<any> {
    const tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set('Authorization', tokenStr);
    return this.httpClient.get("api/products", {headers, responseType: 'text' as 'json' });
  }
}
