import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserItemComponent } from './user-item/user-item.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import {ProductItemComponent} from "./product-item/product-item.component";
import {ProductListComponent} from "./product-list/product-list.component";
import {HomeComponent} from "./home/home.component";
import {CreateQuotationComponent} from "./create-quotation/create-quotation.component";
import {ProductsShoppingCartComponent} from "./products-shopping-cart/products-shopping-cart.component";
import {UserLoginComponent} from "./user-login/user-login.component";
import {CanActivateService} from "./security/can-activate.service";
import {QuotationListComponent} from "./app/quotation-list/quotation-list.component";
import {DoorsComponent} from "./doors/doors.component";
import {WindowsComponent} from "./windows/windows.component";
import {SlidingSystemsComponent} from "./sliding-systems/sliding-systems.component";
import {RatingComponent} from "./rating/rating.component";


//http://localhost:4200/register-user
const routes: Routes = [
  {path: "register-user", component: UserRegistrationComponent},
  {path: "user", component: UserItemComponent, canActivate: [CanActivateService]},
  {path: "users", component: UserListComponent, canActivate: [CanActivateService]},
  {path: "", component: UserLoginComponent},
  {path: "product", component:ProductItemComponent, canActivate: [CanActivateService]},
  {path: "products",component:ProductListComponent, canActivate: [CanActivateService]},
  {path:"home",component: HomeComponent, canActivate: [CanActivateService]},
  {path:"create-quotation/:id",component: CreateQuotationComponent, canActivate: [CanActivateService]},
  {path:"shopping-cart/:id", component: ProductsShoppingCartComponent, canActivate: [CanActivateService]},
  {path:"quotations", component: QuotationListComponent, canActivate: [CanActivateService]},
  {path:"doors", component:DoorsComponent, canActivate: [CanActivateService]},
  {path:"windows", component: WindowsComponent, canActivate: [CanActivateService]},
  {path:"sliding-systems", component: SlidingSystemsComponent, canActivate: [CanActivateService]},
  {path:"rating", component: RatingComponent, canActivate: [CanActivateService]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
