import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {QuotationServiceService} from "../service/quotation-service.service";
import {Router, RouterOutlet} from "@angular/router";
import {QuotationDTO} from "../model/quotationDTO";
import {AuthService} from "../security/auth.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  quotationCreated: QuotationDTO = new QuotationDTO();
  url = '';
  id = 1;

  constructor(private http: HttpClient,
              private quotationService: QuotationServiceService,
              private router: Router,
              private auth: AuthService) {
  }

  ngOnInit(): void {

  }

  createQuotation(): void {
    let quotation: QuotationDTO = {
      id: 0,
      totalPrice: 0,
      productDTOS: [],
      userName: null
    }
    this.quotationService.createQuotation(quotation, this.auth.getToken()).subscribe(response => {
      if (response) {
        this.quotationCreated = JSON.parse(response);
        this.router.navigate(['/create-quotation/' + this.quotationCreated.id]);
      }

    })
  }
  logout(): void{
    this.auth.logout();
  }
  goToQuotations(){
    this.router.navigate(['/quotations']);
  }
  goToDoors(){
    this.router.navigate(['/doors']);
  }
  goToWindows(){
    this.router.navigate(['/windows']);
  }
  goToSlidingSystems(){
    this.router.navigate(['/sliding-systems']);
  }
  goToRating(){
    this.router.navigate(['/rating']);
  }
}
