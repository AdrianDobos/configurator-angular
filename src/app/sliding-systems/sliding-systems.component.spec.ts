import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidingSystemsComponent } from './sliding-systems.component';

describe('SlidingSystemsComponent', () => {
  let component: SlidingSystemsComponent;
  let fixture: ComponentFixture<SlidingSystemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlidingSystemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidingSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
