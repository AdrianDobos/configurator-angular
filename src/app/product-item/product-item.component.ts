import {Component, Input, OnInit} from '@angular/core';
import { ProductDTO } from 'src/app/model/productDTO';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input()
  product: ProductDTO = {
    doorImage:"https://qfortro.mncdn.com/wp-content/uploads/2022/03/usi-PVC-gama-Modern.jpg",
    height:2000,
    width:1000,
    price:1500,
    color:"GOLDEN_OAK",
    productType:"DOOR"
  }

  constructor() { }

  ngOnInit(): void {
    console.log("aici");
  }

}
