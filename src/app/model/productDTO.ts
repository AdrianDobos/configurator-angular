export class ProductDTO {
  height: number;
  width: number;
  price: number;
  color: string;
  productType: string;
  doorImage="https://qfortro.mncdn.com/wp-content/uploads/2022/03/usi-PVC-gama-Modern.jpg";

}
