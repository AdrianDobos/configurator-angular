import {ProductDTO} from "./productDTO";

export class QuotationDTO{
  id: number;
  totalPrice;
  productDTOS: ProductDTO[];
  userName: string;
}
