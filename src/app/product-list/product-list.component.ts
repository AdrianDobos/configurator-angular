import {Component, OnInit} from '@angular/core';
import {ProductDTO} from "../model/productDTO";
import {ProductService} from "../product.service";
import {AuthService} from "../security/auth.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: ProductDTO[] = [];

  constructor(private productService: ProductService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.productService.getAll(this.authService.getToken()).subscribe(responseFromJavaApplication => {
        console.log(responseFromJavaApplication);
        this.products = JSON.parse(responseFromJavaApplication);
      },
      error => {
        alert("We had an error retrieving the data!")
        console.log("Error from server is: ");
        console.log(error)
      });
  }}
