import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthService} from "./auth.service";
import {CanActivate, Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class CanActivateService implements CanActivate{
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isUserLoggedIn()) {
      this.router.navigate(['']);
      console.log('ceva')
      return false;
    }else{
      console.log('ceva2')
      return true;
    }

  }
}
