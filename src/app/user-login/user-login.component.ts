import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserCreateDTO } from '../model/userCreateDTO';
import { UserService } from '../user.service';
import {AuthService} from "../security/auth.service";
import {UserDTO} from "../model/userDTO";
import {FormControl, FormGroup} from "@angular/forms";
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  errorMessage = 'Invalid Credentials';
  successMessage: string;
  invalidLogin = false;
  loginSuccess = false;
  user: UserDTO = new UserDTO();

  username = "";

  password = "";

  constructor(private userService: UserService,
              private router: Router,
              private authenticationService: AuthService) { }

  ngOnInit(): void {
  }

  onClick() {
    if (this.form){
      this.user.password = this.form.get('password').value;
      this.user.userName = this.form.get('username').value;
      if (this.user){
        this.authenticationService.authentication(this.user).subscribe(result => {
          this.authenticationService.TOKEN_SESSION_ATTRIBUTE_NAME = result.token;
          if (this.user?.userName){
            this.authenticationService.USER_NAME_SESSION_ATTRIBUTE_NAME = this.user?.userName;
          }

          console.log(this.authenticationService.USER_NAME_SESSION_ATTRIBUTE_NAME);
          console.log(result.token);
          this.authenticationService.registerSuccessfulLogin(this.user.userName);
          this.invalidLogin = false;
          this.loginSuccess = true;
          this.router.navigate(['home']);
        }, () => {
          this.invalidLogin = true;
          this.loginSuccess = false;
        });
      }
    }

    }


  goToRegister() {
    this.router.navigate(['register-user'])
  }
}

