import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {QuotationServiceService} from "../service/quotation-service.service";
import {QuotationDTO} from "../model/quotationDTO";
import {AuthService} from "../security/auth.service";

@Component({
  selector: 'app-create-quotation',
  templateUrl: './create-quotation.component.html',
  styleUrls: ['./create-quotation.component.css']
})
export class CreateQuotationComponent implements OnInit {
  id?: number;
  productsNumberOfQuotation = 0;
  quotation?: QuotationDTO;
  window = 'WINDOW';
  door = 'DOOR';
  slidingSystem = 'SLIDING_SYSTEM';
  seeWindowForm = false;
  seeDoorForm = false;
  seeSlidingSystemForm = false;
  addWindow = "Add window";
  addDoor = "Add door";
  addSlidingSystem = "Add sliding system"
  totalPriceQuotation = 0;

  constructor(private route: ActivatedRoute,
              private quotServ: QuotationServiceService,
              private  router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    if (this.id){
      this.quotServ.getQuotation(this.id, this.authService.getToken()).subscribe(response => {
        this.quotation = JSON.parse(response);
        if (this.quotation.productDTOS){
          this.productsNumberOfQuotation = this.quotation.productDTOS.length;
        }
        if (this.quotation.totalPrice){
          this.totalPriceQuotation = this.quotation.totalPrice;
        }
      })
    }
  }

  seeWindow(): void {
    this.seeWindowForm = true;
    this.seeSlidingSystemForm = false;
    this.seeDoorForm = false;
  }


  seeDoor(): void {
    this.seeDoorForm = true;
    this.seeWindowForm = false;
    this.seeSlidingSystemForm = false;

  }

  seeSlidingSystem(): void {
    this.seeWindowForm = false;
    this.seeSlidingSystemForm = true;
    this.seeDoorForm = false;

  }

  goToShoppingCart(): void{
    this.router.navigate(['shopping-cart/' + this.id]);
  }

}
