import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {ProductService} from "../service/product.service";
import {QuotationServiceService} from "../service/quotation-service.service";
import {QuotationDTO} from "../model/quotationDTO";
import {AuthService} from "../security/auth.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input()
  quotationId?: number;
  @Input()
  productType?: string;
  @Input()
  addProductType?: string;
  @Output()
  numberOfProductsEvent : EventEmitter<number> = new EventEmitter();
  @Output()
  closeProductEvent : EventEmitter<boolean> = new EventEmitter();
  @Output()
  totalPriceEvent : EventEmitter<number> = new EventEmitter();
  completeFormsMessage = false;
  quotation: QuotationDTO = new QuotationDTO();

  width = new FormControl('', [Validators.required]);
  height = new FormControl('', [Validators.required]);
  color = new FormControl('', [Validators.required]);
  constructor(private prodService: ProductService,
              private quotService: QuotationServiceService,
              private auth: AuthService) { }

  ngOnInit(): void {
  }
  saveProduct(): void{
    const product = {
      id: undefined,
      color: this.color.value,
      productType: this.productType,
      width: this.width.value,
      height: this.height.value,
      quotationId: this.quotationId
    }
    if (this.height.valid && this.width.valid && this.color.valid){
      this.prodService.createProduct(product, this.auth.getToken()).subscribe(res => {
        if (this.quotationId){
          this.quotService.getQuotation(this.quotationId, this.auth.getToken()).subscribe(quot => {

           this.quotation = JSON.parse(quot);
            this.numberOfProductsEvent.emit(this.quotation.productDTOS?.length);
            this.closeProductEvent.emit(false);
            if (this.quotation.totalPrice){
              this.totalPriceEvent.emit(this.quotation.totalPrice)
            }
            console.log('succeeded');
          });
        }

      });
    }else{
      this.height.markAsTouched();
      this.width.markAsTouched();
      this.color.markAsTouched();
      this.completeFormsMessage = true;
    }
  }

  checkIfCompleteMessageIsTrueAndTheFormsAreCompletedMakeItFalse(){
    if (this.completeFormsMessage){
      if (this.height.valid && this.width.valid && this.color.valid){
        this.completeFormsMessage = false;
      }
    }
  }
}
